#include <Stepper.h>

char receivedChar;
boolean newData = false;

// number of steps to move
// 8 is one step when 400 pulse/rev setup is used from the driver
// one step moves 20 micrometer
// 360/0.9 degree gives you 400 steps/rev
// Hence, 360 degree rotation is 8*400
const int onestep = 8;
const int onerotation = 400;

const int spreader = onestep*onerotation*10;
const int spreader2 = onestep*onerotation*4.5;
const int spreader3 = onestep*onerotation*7.5;
const int platform_supply = onestep*5*10; // required steps for powder spreading
const int platform_build = onestep*5;
const int platform_supply2 = onestep*onerotation;

// initialize the stepper library on pins 2-5 n 8-11
Stepper myStepper1(spreader,2,3); // Powder spreader motor pin connection
Stepper myStepper2(platform_supply,8,9); // Powder supply platform motor pin connection
Stepper myStepper3(platform_build,12,11); // Build platform motor pin connection

void setup() {
    // initialize the serial port:
    Serial.begin(9600);
    // set the speed of each motor in RPM:
    myStepper1.setSpeed(10); //Powder spreader motor
    myStepper2.setSpeed(60); //Powder reservoir platform motor
    myStepper3.setSpeed(60); //Build platform motor
    Serial.println("Arduino is ready");
    Serial.println("To spread the powder, type: A");
    Serial.println("To raise the powder supply platform by 1000 micrometer, type: B");
    Serial.println("To lower the build platform by 100 micrometer, type: C");
    Serial.println("To lower the powder supply platform by 1000 micrometer, type: D");
//    Serial.println("To start the entire sequence, type: E");
//    Serial.println("  > this will lower and raise the build and \n supply platform 100 micrometer, and spread the powder");
    Serial.println("To raise the build platform by 100 micrometer, type: F");
    Serial.println("To spread the powder on the supply platform, type: G");
    Serial.println("  >  Use this when you need to flatten the powder on the platform");
    Serial.println("");
}

void loop() {
    recvOneChar();
    showNewData();
    if (receivedChar == 'A') {
        Serial.println("Spreading powder...");
        // *** Powder Spreader motion control ************
        myStepper1.step(spreader); // move the motor in clockwise direction for declared number of steps
        myStepper1.step(spreader2);
        delay(100); // delay the execution of the next line of code for microseconds
        myStepper1.step(-spreader); // move the motor in counterclockwise direction for declared number of steps
        myStepper1.step(-spreader2);
        delay(500);
        // ************************************************
     }
     else if (receivedChar == 'B') {
        Serial.println("raising the powder supply platform...");
        myStepper2.step(-platform_supply);
        delay(100);
     }
     else if (receivedChar == 'C') {
        Serial.println("lowering the build platform...");
        myStepper3.step(platform_build);
        delay(100);
     }
     else if (receivedChar == 'D') {
        Serial.println("lowering the supply platform...");
        myStepper2.step(platform_supply);
     }
//     else if (receivedChar == 'E') {
//        Serial.println("Executing the entire sequence...");
//        myStepper1.step(spreader); // move the motor in clockwise direction for declared number of steps
//        myStepper1.step(spreader2);
//        delay(100); // delay the execution of the next line of code for microseconds
//        myStepper1.step(-spreader); // move the motor in counterclockwise direction for declared number of steps
//        myStepper1.step(-spreader2);
//        delay(500);
//        myStepper2.step(-platform);
//        myStepper3.step(platform);
//     }
    else if (receivedChar == 'F') {
        Serial.println("raising the build platform...");
        myStepper3.step(-platform_build);
        delay(500);
     }
    else if (receivedChar == 'G') {
        Serial.println("spreading the powder on the supply platform...");
        myStepper1.step(-spreader3);
        delay(500);
     }
}

void recvOneChar() {
    if (Serial.available() > 0) {
        receivedChar = Serial.read();
        newData = true;
    }
}

void showNewData() {
    if (newData == true) {
        Serial.print("This just in ... ");
        Serial.println(receivedChar);
        newData = false;
    }
}
