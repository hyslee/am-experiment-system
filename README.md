# Additive Manufacturing Experiment System

- The documentation is split into three sections. This page includes general information about the list of items required, operation procedure, assembly, and maintenace. The descriptions of the Arduino code and Solidworks CAD files can be found in Arduino and Solidworks folders in this project.
- Please submit an issue if you have/find any problems. This will make it easier to keep track of them.

## System Overview

![overview1](/pictures/overview1.jpg)
![overview2](/pictures/overview2.jpg)

## Required Components

### Motion System 
#### Servocity (www.servocity.com)

|     Item                                                                                 |     Part no.          |     Qty    |     Unit price    |
|------------------------------------------------------------------------------------------|-----------------------|------------|-------------------|
|     U-Channel (9 Hole, 7.50"   Length)                                                   |     585448            |     3      |     $ 2.80        |
|     7.50" X-Rail                                                                         |     565034            |     4      |     $ 1.80        |
|                                        9.00" X-Rail                                      |     565038            |     2      |     $ 1.89        |
|     3.75" X-Rail                                                                         |     565022            |     2      |     $ 1.18        |
|     X-Rail Nut (4 pack)                                                                  |     585756            |     4      |     $ 2.00        |
|     X-Rail L-Bracket (2 pack)                                                            |     585076            |     2      |     $ 1.99        |
|     3 Hole Pattern Plate (1.50" x   3.00")                                               |     585720            |     3      |     $ 1.99        |
|     11.81" (300mm) 8mm Lead Screw                                                        |     3501-0804-0300    |     3      |     $ 7.59        |
|     8mm x 200mm Stainless Steel   Precision Shafting                                     |     2100-0008-0200    |     3      |     $ 3.69        |
|     8mm Lead Screw Barrel Nut                                                            |     545314            |     3      |     $ 4.99        |
|     3504 Series Lead Screw Clamping   Collar (8mm Lead, 4 Start, 21mm OD, 9mm Length)    |     3504-0804-2109    |     6      |     $ 4.99        |
|     8mm ID x 24mm Length Linear Ball   Bearings (2 Pack)                                 |     1612-0815-0024    |     1      |     $ 4.99        |
|     8mm ID x 45mm Length Linear Ball   Bearings (2 Pack)                                 |     1612-0815-0045    |     1      |     $ 7.99        |
|     1613 Series Thrust Ball Bearing   (8mm ID x 16mm OD, 5mm Thickness)                  |     1613-0516-0008    |     6      |     $ 3.99        |
|     1/2" Bore Bottom Tapped   Clamping Mount                                             |     585546            |     3      |     $ 5.99        |
|     Aluminum Flat Beam (7 Hole,   2.31" Length) - 2 Pack                                 |     585406            |     2      |     $ 1.60        |
|     Beam Attachment Blocks B (4 pack)                                                    |     585600            |     2      |     $ 4.99        |
|     8mm Bore Side Tapped Pillow Block                                                    |     535094            |     6      |     $ 3.20        |
|     8mm Bore Bottom Tapped Pillow Block                                                  |     535104            |     6      |     $ 3.00        |
|     5mm to 8mm Flexible Clamping Shaft   Coupler                                         |     4002-0005-0008    |     3      |     $ 4.99        |
|     NEMA 14 Stepper Motor Mount                                                          |     555156            |     3      |     $ 2.80        |
|     Side Tapped Pattern Mount B                                                          |     545424            |     3      |     $ 4.99        |
|     Single Screw Plate (24 pack)                                                         |     585474            |     1      |     $4.99         |
|     X-Rail Screw Plate (2 pack)                                                          |     585757            |     4      |     $4.99         |

#### STEPPERONLINE (https://www.omc-stepperonline.com/)

|     Item                                                                          |     Part no.    |     Qty    |     Unit price    |
|-----------------------------------------------------------------------------------|-----------------|------------|-------------------|
| Nema 14 Bipolar 0.9deg 11Ncm (15.58oz.in) 0.4A 10V 35x35x28mm 4 Wires             | 14HM11-0404S    | 3          | $13.82            |
| Digital Stepper Driver 0.3-2.2A 10-30VDC for Nema 8, 11, 14, 16, 17 Stepper Motor | DM320T          | 3          | $17.00            |

#### Arduino (www.arduino.cc)

|     Item         |     Part no.    |     Qty    |     Unit price    |
|------------------|-----------------|------------|-------------------|
| Arduino Uno REV3 | A000066         | 2          | $29.95            |



#### Custom Made
|     Item       |     Part no.    |     Qty    |     Unit price    | Note                                   |
|----------------|-----------------|------------|-------------------|----------------------------------------|
| Powder Bed     |                 | 1          |                   | Made of aluminum                                   |
| Build Platform |                 | 2          |                   | Made of aluminum                                   |
| Vacuum lid     |                 | 1          | $400              | Made of cast acrylic sheet 1 in. thick(flexiglass) |
| Substrates     |                 | 2          |                   | Carbon Steel                                       |
| Pipe Coupling 3/4" |             | 1          |                   | Brass                                          |
| Pipe Coupling 1/2" |             | 2          |                   | Stainless Steel                                |


* Substrates are made of leftover carbon steel plates from Honda project
### Vaccum Chamber

#### BVV (shopbvv.com)
|     Item                                                                                |     Part no.    |     Qty    |     Unit price    |
|-----------------------------------------------------------------------------------------|-----------------|------------|-------------------|
| [7 Gallon Aluminum - POT ONLY](https://shopbvv.com/products/7-gallon-aluminum-pot-only) | 55270           | 1          | $150.00           |

#### Oxygen Sensor

| Item                           | Part no.    | Qty | Unit Price | Note |
|--------------------------------|-------------|-----|------------|------|
| [The Gravity: I2C Oxygen Sensor](https://www.dfrobot.com/product-2052.html) | SKU:SEN0322 | 1   | $53.90     |      |

### McMaster-Carr

| Item                                                                                                    | Part no. | Qty | Unit Price   | Note           |
|---------------------------------------------------------------------------------------------------------|----------|-----|--------------|----------------|
| Insertion Heater with Internal Temperature Sensor for 1/4" Hole, 120V AC, 3" Long Heating Element, 200W | 8440T121 | 2   | $94.95 Each  |                |
| Precision Programmable Temperature Controller with Relay Output, 1/16 DIN                               | 38615K71 | 2   | $188.04 Each | Extech 48VFL11, [manual](http://www.extech.com/products/resources/48VFL_96VFL_UM-en.pdf) |
| Black-Oxide Alloy Steel Socket Head Screw M3 x 0.5 mm Thread, 6 mm Long                               | 91290A111 | 1   | $9.89 per pack of 100 |  |

* ***You may need to buy additional items***

## Maintenance & Precautions

### AM Motion System

 - Most of the components of the motion system are made of aluminum, so you have to bare in mind that the safe operating temperature is around 400 fahrenheit. Exceeding the operating tempature may cause damge to the system.
 - The powder frame is made of aluminum 2025, it will lose half of its tensile strength at 400 degree fahrenheit. The Anti-seize grease need to be applied on the cartridge heaters before you insert them to prevent getting stuck in the slots during the heating process. The temperature of catridge heaters should not go above the operating temperature, otherwise it may cause local melting at the contact area.
 - Do not apply excessive torque on Beam Attachment Blocks B, the screw thread is soft and most likely going to fail first. 

 ![blockB](/pictures/beamblockB.jpg)

- The stepper motors run hot, they will hover between 70 and 75 degree Celsius (158 and 167 F). The magnetic core of the motors would start to degrade if the core temperature goes above 80 degree Celsius (176 F). The overheatng will cause the reduction of the torque output. Cutting the power off to cool down the motor may be necessary during the system operation. For more information, see [reference article](http://www.sd3d.com/stepper-motor-cooling/)

### Powder Bed Frame

  - Magnetic pedestals use a strong neodymium magnet, caution is advised.

### Vacuum Chamber Cover

 - The cover is made of cast acrylic and prone to damage from chemcials such as acetone and ethanol. Also, prolonged exposure to UV lights can cause changes to the color or property of the material.   
 - Expected lifespan of the cover is around 4 years (fabricated on 2021/09/30) according to Machining and Technical Servies (MTS). After that period of time, a proper sealing of vacuum chamber can not be guaranteed.
 - Applying petroleum jelly-based product (Vaseline, vacuum grease) on the rubber gasket is highly recommended to ensure a proper sealing. Due to the slight misalignment in between the cover gasket and the aluminum pot, leakage may occur through the gap (Neither the aluminum pot nor the cover is perfectly round).

### Oxygen Sensor

 - Due to the nature of the electrochemcial oxygen sensors, the sensor would either stop working or measure inaccurate values. The lifespan of the oxygen sensor is around 2 years (opened on 2021/11/05, manufactured on 2021/07/01), and the chemical reaction would cease to occur after that period. For more information, see [reference article](https://gaslab.com/blogs/articles/how-does-an-oxygen-sensor-work)
 - I highly recommend to read the manufacturer's [documentation](https://wiki.dfrobot.com/Gravity_I2C_Oxygen_Sensor_SKU_SEN0322) and be aware of the precautions provided by the manufacturer.

### Temperature PID controller

 - The PID controller draws power directly from a power outlet. Caution is required. 
 - The controller is a programmable unit, follow the manufacturer's [guidelines](http://www.extech.com/products/resources/48VFL_96VFL_UM-en.pdf) for the PID programming. 

### Metal Powders

  - You MUST wear a P100 graded respirator mask when you are handling metal powders. Inhaling excessive amount of air-bone powder may cause respiratory illness.

## Design Decisions

### Doctor Blades
  - [S.Haeri](https://www.sciencedirect.com/science/article/pii/S0032591017306551) conducted a blade geometry optimization study by utilizing discrete element method (DEM). The study proposed that the tip of the blade shape can be optimized as a function of three paramters to achieve the highest layer density; $`n_s`$, $`a_s`$ and $`b_s`$ respectively, control the overall shape, width and height of the profile; the geometry of the tip is given by the following equation. 
```math
 |\frac{y}{a_s}|^{n_s} + |\frac{z}{b_s}|^{n_s} = 1 
```
  This work has not been tested on the current system due to time constraint.

  - [Lin Wang et al.](https://www.sciencedirect.com/science/article/pii/S003259102100125X) proposed that a round blade tip has shown to cause the least powder layer segregation and deposit the most amount of powder on each layer. Based on that, two types of doctor blade tips are fabricated: O-ring and flat-head. O-ring and flat-head are to test the round and wide blade geomtry on the paper. 
  - In-house test revealed that the O-ring blade is more effective in terms of spreading the average 44 micron size iron powder evenly. A powder layer spreaded by the flat-head blade showed segregation repeatedly in multiple trials.

### Vacuum Chamber Cover
  - The cover is and made by Machinining and Technical Services (MTS) in Clemson University. The raw material used for the cover is 1" thick cast acrylic sheet. A 3D printed part is bolted on to prevent the slip on the vaccum pot and a rubber gasket is added to make a proper seal. For the gas inlet and outlet, two 1/4 NPT barbed fittings are installed; The pipe size of the fittings is chosen to be as small as possible to minimize the impact of structural integrity.
  - The laser window slot was originally designed to be a detachable assembly, but satisfying acceptable vacuum pressure could not be reached. Hence, the glass window is permenantly attached on the cover with RTV silicone gasket. 

### Vacuum Hose
  - [High-pressure PVC plastic tubing](https://www.mcmaster.com/5238K648/) is used for the vacuum line to prevent collapse.

### Cartridge Heaters
  - The watt density of the cartridge heaters is decided based on the material of the power bed frame (Al-2025 alloy). The operating temperature is set as 400 fahrenheit and the maximum watt density is decided as 200 W/in. For more information, see [reference](https://www.chromalox.com/en/resources-and-support/design-tools/designguidelines/cartridgeheatersselectionapplicationsandinstallationguidelines)

  ![gap](/pictures/heater_gap.jpg)

  ![wattdensity](/pictures/heater_watt_density2.jpg)

  - Two 3 inch long 1/4" cartridge heater are installed on the powder bed frame to ensure even heating.
  - I ***DO NOT*** recommend raising the temperature above 400 fahrenheit due to the fact that the catridge heater and powder bed frame are made of different materials (stainless steel and aluminum alloy). The difference in thermal expansion rate may lead to local melting. 

  
### Powder Build Substrates 
  - There is only one available build substrate made of carbon steel. I advise not to emit laser directly on the substrate as heat will cause thermal deformation, rendering it unusable. 

### Powder Bed Frame 
  - Magnetic pedestals are installed to hold the powder bed frame in place. Initial testing of the system revealed that the bed frame was being moved around slightly during the spreading process because of the pressure applied by the spreading (doctor) blade. 

### Wire Feedthroughs
  - The wall thickness of the aluminum pot was too thin to tab screw threads, thereby the adapters for pipe fitting couplings were installed on the pot and holes were drilled for the wires. High temperature J-B metallic paste is used to permenantly attach the adapters on the pot.
  - Epoxy adhesives are used to fill the wire holes. 

### Glass Window
  - [50 x 50mm VIS-NIR Coated, 1λ Fused Silica Window](https://www.edmundoptics.com/p/50-x-50mm-vis-nir-coated-1lambda-fused-silica-window/10161/) is installed on the vacuum cover.

### Oxygen Sensor

  - The oxygen sensor data output is available through Arduino serial monitor. Make sure to select the correct port. 
  - The arduino code is available in the manufacture's documentation page [(link)](https://wiki.dfrobot.com/Gravity_I2C_Oxygen_Sensor_SKU_SEN0322) 

### Stepper Motor Drivers (DM320T)

  - The motor driver is set to put out 1.0A peak and 0.71A RMS current, and 400 Pulse/rev. The current setup is set based on the stepper motor sepcification, but Pulse/rev configuration is found through trial and error; it is configured to generate enough torque to move the weight of the platform motion system without stuttering. You can modify the driver configuration, but you need to keep in mind that modifying the Pulse/rev configuration would necessitate modification of the Arduino stepper motor code. 

## Assembly

### Preparing Components 

- For more information, see Solidworks folder. 

 1. Servocity 6 holes U-channel components are cut to length of 4.93"
 2. Leadscrew 200mm is cut to length of 4.86" for supply motion and 4.6" for build motion system
 3. 8mm round Stainless Steel Precision Shaftings for supply and build motion system are cut to length of 5.7"
 4. X-Rail T-slot extrusions used for mounting the spreading system are cut to length of 5.5" 
 5. Some of beam attachment block B screw threads are removed.  

![beamb1](/solidworks_pics/beamblock_exp.jpg)

### Instructions
  
  - I recommend you to follow this sequence when you assemble the system.
  - For more information, see Solidworks folder. 

1. Assemble the base frame
3. Assemble the powder spreader motion system (horizontal_motion1) and mount it on the base frame 
2. Assemble the support structure for the spreader (horizontal_motion2) and mount it on the base frame
4. Assemlbe the build and powder supply motion systems
5. Mount the platform support beams on the 3-hole pattern plates used for the build and supply motion systems and assemble the square center piece.
6. Place the assembled motion system in the vacuum pot
7. Place the powder bed frame
8. Assemble the doctor (powder) blade
9. Apply vacuum grease on the vacuum cover rubber gasket and place it on the pot
10. Apply thread seal (teflon) tape on barbed fitting screw threads and screw it down tightly for a proper seal. 

### Wiring

#### Stepper Motor 

![alt text](/pictures/wiring.jpg)

##### Cartridge Heater

![heater](/pictures/heater.jpg)
