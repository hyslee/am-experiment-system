# Solidworks Version

- You must use Solidworks 2021 or higher versions

- Exploded views and animations of each subassemblies are available as Solidwork configurations

![configuration](/solidworks_pics/conf.png)

# System Overview

![](/solidworks_pics/system.JPG)

# Base Frame Exploded View

| Item                                                        | Qty. | Note |
|-------------------------------------------------------------|------|------|
| 7.50" X-Rail                                                | 2    |      |
| 9.00" X-Rail                                                | 2    |      |
| X-Rail L-Bracket                                            | 4    |      |
| X-Rail Screw Plate                                          | 8    |      |
| 6-32 x 0.3125" (5/16) Zinc-Plated Socket Head Machine Screw | 24   |      |

![frame](/solidworks_pics/frame.JPG)

# Spreading Motion System Exploded View

| Item                                                                                                             | Qty | Note                    |
|------------------------------------------------------------------------------------------------------------------|-----|-------------------------|
| STEPPERONLINE Nema 14 Stepper Motor 0.9deg(400 Steps/rev) 0.4A 11Ncm/15.6oz.in Bipolar 4-Wires                   | 1   |                         |
| 7.50" X-Rail                                                                                                     | 2   | cut to a length of 5.5" |
| X-Rail T-Bracket (2 pack)                                                                                        | 1   |                         |
| 6-32 x 0.3125" (5/16) Zinc-Plated Socket Head Machine Screw                                                      | 14  |                         |
| 6-32 x 0.375" (5/16) Zinc-Plated Socket Head Machine Screw                                                       | 27  |                         |
| U-Channel (13 Hole, 10.50" Length)                                                                               | 1   |                         |
| 1613 Series Thrust Ball Bearing (8mm ID x 16mm OD, 5mm Thickness)                                                | 2   |                         |
| 1611 Series Flanged Ball Bearing (8mm ID x 14mm OD, 5mm Thickness) - 2 Pack                                      | 1   |                         |
| 5mm to 8mm Flexible Clamping Shaft Coupler                                                                       | 1   |                         |
| 8mm Bore Side Tapped Pillow Block                                                                                | 2   |                         |
| 8mm Bore Bottom Tapped Pillow Block                                                                              | 2   |                         |
| 8mm x 250mm Stainless Steel Precision Shafting                                                                   | 1   |                         |
| 3501 Series Lead Screw (8mm Lead, 4 Start, 250mm Length)3501 Series Lead Screw (8mm Lead, 4 Start, 250mm Length) | 1   |                         |
| 3504 Series Lead Screw Clamping Collar (8mm Lead, 4 Start, 21mm OD, 9mm Length)                                  | 1   |                         |
| 3 Hole Pattern Plate (1.50" x 3.00")                                                                             | 1   |                         |
| 1/2" Bore Bottom Tapped Clamping Mount                                                                           | 2   |                         |
| 8mm Lead Screw Barrel Nut                                                                                        | 1   |                         |

![motion1](/solidworks_pics/motion1.JPG)

| Item                                                             | Qty | Note                    |
|------------------------------------------------------------------|-----|-------------------------|
| 7.50" X-Rail                                                     | 2   | cut to a length of 5.5" |
| 9" X-Rail                                                        | 1   |                         |
| X-Rail T-Bracket (2 pack)                                        | 1   |                         |
| 6-32 x 0.3125" (5/16) Zinc-Plated Socket Head Machine Screw      | 14  |                         |
| X-Rail Nut (4 pack)                                              | 2   |                         |
| 3/8" x 3/8" 90° X-Rail Bracket (2 pack)                          | 1   |                         |
| 3 Hole Pattern Plate (1.50" x 3.00")                             | 1   |                         |
| Mini V-Wheels (2 pack)                                           | 2   |                         |
| Mini V-Wheel Standoff (4 pack)                                   | 1   |                         |
| 6-32 Stainless Steel Button Head Screw (0.3125" Length) - 6 Pack | 1   |                         |

![motion2](/solidworks_pics/motion2.JPG)

# Build Platform Motion System Exploded View

| Item                                                                                           | Qty | Note                     |
|------------------------------------------------------------------------------------------------|-----|--------------------------|
| 3.50" X-Rail                                                                                   | 1   |                          |
| 6-32 x 0.3125" (5/16) Zinc-Plated Socket Head Machine Screw                                    | 12  |                          |
| 6-32 x 0.375" (5/16) Zinc-Plated Socket Head Machine Screw                                     | 21  |                          |
| X-Rail Nut (4 pack)                                                                            | 2   |                          |
| 3/8" x 3/8" 90° X-Rail Bracket (2 pack)                                                        | 1   |                          |
| 3 Hole Pattern Plate (1.50" x 3.00")                                                           | 1   |                          |
| Black-Oxide Alloy Steel Socket Head Screw M3 x 0.5 mm Thread, 6 mm Long                        | 4   | McMaster                 |
| STEPPERONLINE Nema 14 Stepper Motor 0.9deg(400 Steps/rev) 0.4A 11Ncm/15.6oz.in Bipolar 4-Wires | 1   |                          |
| U-Channel (7 Hole, 6.00" Length)                                                               | 1   | cut to a length of 4.93" |
| 8mm Lead Screw Barrel Nut                                                                      | 1   |                          |
| 5mm to 8mm Flexible Clamping Shaft Coupler                                                     | 1   |                          |
| 8mm Bore Side Tapped Pillow Block                                                              | 2   |                          |
| 8mm Bore Bottom Tapped Pillow Block                                                            | 2   |                          |
| 8mm x 200mm Stainless Steel Precision Shafting                                                 | 1   | cut to a length of 5.7"  |
| 11.81" (300mm) 8mm Lead Screw                                                                  | 1   | cut to a length of 4.6"  |
| Aluminum Flat Beam (7 Hole, 2.31" Length) - 2 Pack                                             | 1   |                          |
| Beam Attachment Blocks B (4 pack)                                                              | 2   |                          |
| Beam Attachment Blocks B                                                                       | 1   | Screw thread is removed  |
| 8mm ID x 45mm Length Linear Ball Bearings                                                      | 1   |                          |
| 1/2" Bore Bottom Tapped Clamping Mount                                                         | 1   |                          |
| 15mm Bore Bottom Tapped Clamping Mount                                                         | 1   |                          |
| 18-8 Stainless Steel Male Hex Thread Adapter 1/4" OD, 1/4" Long, 6-32 Thread Size              | 2   | McMaster                 |
| Low-Strength Steel Thin Nylon-Insert Locknut, Zinc-Plated, 6-32 Thread Size                    | 1   | McMaster                 |
| NEMA 14 Stepper Motor Mount                                                                    | 1   |                          |
| Side Tapped Pattern Mount B                                                                    | 1   |                          |
| 2" x 2" Center Powder Plate                                                                    | 1   | Custom Made              |
| 2" x 2" Substrate                                                                              | 1   | Custom Made              |

![build](/solidworks_pics/buildmotion.JPG)

# Powder Supply Platform Motion System Exploded View

| Item                                                                                           | Qty | Note                     |
|------------------------------------------------------------------------------------------------|-----|--------------------------|
| 3.50" X-Rail                                                                                   | 1   |                          |
| 6-32 x 0.3125" (5/16) Zinc-Plated Socket Head Machine Screw                                    | 12  |                          |
| 6-32 x 0.375" (5/16) Zinc-Plated Socket Head Machine Screw                                     | 21  |                          |
| X-Rail Nut (4 pack)                                                                            | 2   |                          |
| 3/8" x 3/8" 90° X-Rail Bracket (2 pack)                                                        | 1   |                          |
| 3 Hole Pattern Plate (1.50" x 3.00")                                                           | 1   |                          |
| Black-Oxide Alloy Steel Socket Head Screw M3 x 0.5 mm Thread, 6 mm Long                        | 4   | McMaster                 |
| STEPPERONLINE Nema 14 Stepper Motor 0.9deg(400 Steps/rev) 0.4A 11Ncm/15.6oz.in Bipolar 4-Wires | 1   |                          |
| U-Channel (7 Hole, 6.00" Length)                                                               | 1   | cut to a length of 4.93" |
| 8mm Lead Screw Barrel Nut                                                                      | 1   |                          |
| 5mm to 8mm Flexible Clamping Shaft Coupler                                                     | 1   |                          |
| 8mm Bore Side Tapped Pillow Block                                                              | 2   |                          |
| 8mm Bore Bottom Tapped Pillow Block                                                            | 2   |                          |
| 8mm x 200mm Stainless Steel Precision Shafting                                                 | 1   | cut to a length of 5.7"  |
| 11.81" (300mm) 8mm Lead Screw                                                                  | 1   | cut to a length of 4.86" |
| Aluminum Flat Beam (7 Hole, 2.31" Length) - 2 Pack                                             | 1   |                          |
| Beam Attachment Blocks B (4 pack)                                                              | 2   |                          |
| Beam Attachment Blocks B                                                                       | 1   | Screw thread is removed  |
| 8mm ID x 45mm Length Linear Ball Bearings                                                      | 1   |                          |
| 1/2" Bore Bottom Tapped Clamping Mount                                                         | 1   |                          |
| 15mm Bore Bottom Tapped Clamping Mount                                                         | 1   |                          |
| 18-8 Stainless Steel Male Hex Thread Adapter 1/4" OD, 1/4" Long, 6-32 Thread Size              | 2   | McMaster                 |
| Low-Strength Steel Thin Nylon-Insert Locknut, Zinc-Plated, 6-32 Thread Size                    | 1   | McMaster                 |
| NEMA 14 Stepper Motor Mount                                                                    | 1   |                          |
| Side Tapped Pattern Mount B                                                                    | 1   |                          |
| 2" x 2" Center Powder Plate                                                                    | 1   | Custom Made              |
| 2" x 2" Substrate                                                                              | 1   | Custom Made              |

![powder](/solidworks_pics/supplymotion.JPG)

# Doctor Blade Assembly Exploded View

| Item                                                       | Qty | Note        |
|------------------------------------------------------------|-----|-------------|
| Blade                                                      | 1   | Custom Made |
| 6-32 x 0.375" (5/16) Zinc-Plated Socket Head Machine Screw | 6   |             |
| Blade Mount                                                | 1   | Custom Made |

![blade](/solidworks_pics/blade.JPG)

# Vacuum Chamber Exploaded View

| Item                                                                                                          | Qty | Note                    |
|---------------------------------------------------------------------------------------------------------------|-----|-------------------------|
| 7 Gallon Aluminum - POT ONLY                                                                                  | 1   | BVV                     |
| Fitting Connector Adapters                                                                                    | 3   | Custom Made             |
| 50 x 50mm VIS-NIR Coated, 1λ Fused Silica Window                                                              | 1   |                         |
| Glass Holder                                                                                                  | 1   | Custom Made, 3D printed |
| Zinc-Plated Steel Barbed Hose Fitting for Air and Water, Adapter for 3/8" Hose ID, 1/4 NPT Male               | 2   | McMaster                |
| Precision Extreme-Pressure 316 Stainless Steel Fitting Straight Connector with Hex, 1/2 NPT Male, 1-7/8" Long | 3   | McMaster                |

![vacuum](/solidworks_pics/vacuumchamber.JPG)
